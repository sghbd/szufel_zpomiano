# Lodówki na oleole.pl
Program pobiera dane na temat lodówek ze sklepu _oleole.pl_, strukturyzuje dane oraz
wprowadza je do lokalnej bazy danych.
Następnie program potrafi stworzyć prosty raport interaktywny pozwalający przejrzeć
proste statystyki.

![](screen.png)

**Testowane na Ubuntu 16.04 LTS**

Student: [Zbigniew Pomianowski](mailto:zbigniew.pomianowski@gmail.com)

### Instalacja
~~~~
:::bash
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
virtualenv venv
source venv/bin/activate
git clone https://bitbucket.org/sghbd/szufel_zpomiano.git
cd szufel_zpomiano
pip install -r requirements.text
~~~~

### Użycie
Aktywacja środowiska wirtualnego pythona:
~~~~
:::bash
source venv/bin/activate
~~~~
Tryb _kopania_ danych ze sklepu - uruchomienie pająka:
~~~~
:::bash
python main.py -m
~~~~
Tryb _wyświetlania_ danych w postaci strony internetowej:
~~~~
:::bash
python main.py -s
~~~~
Tryb _interaktywnej konsoli_ - pozwala np. robić zapytania SQL via pyDAL:
~~~~
:::bash
python main.py --shell
~~~~

### Pomoc
~~~~
:::bash
python main.py -h
~~~~