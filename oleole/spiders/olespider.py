# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.linkextractors import LinkExtractor
from ..items import Product
from db import db


def sstrip(s):
    return s.replace('\n', '').replace('\t', '').strip()


class OleSpider(scrapy.Spider):
    name = "oleole"

    allowed_domains = ['oleole.pl']
    start_urls = [
        r.url for r in db(db.t_product.url != None).select(db.t_product.url)]
    start_urls += ['http://www.oleole.pl/lodowki.bhtml']

    item_map = {
        u'Ilość agregatów': 'aggre_nb',
        u'Ilość termostatów': 'therm_nb',
        u'Bezszronowa (No Frost)': 'nofrost',
        u'Pojemność netto chłodziarki': 'capacity_refrig',
        u'Pojemność netto zamrażarki': 'capacity_freeze'
    }

    ### Dla CrawlSpider
    ### Wymaga jednak dodatkowego parsowania wyekstraktowanych linków
    # rules = [
    #     scrapy.spiders.Rule(
    #         LinkExtractor(
    #             allow=('lodowk.*', )),
    #         callback="parse",
    #         follow=True)
    # ]

    def parse(self, response):
        for url in (response.xpath(
                '//div[@class="paging-numbers"]//a/@href').extract() +
                response.css('.product-name').xpath('//a/@href').extract()):
            try:
                yield scrapy.Request(url, callback=self.parse)
            except:
                yield scrapy.Request(response.urljoin(url),
                                     callback=self.parse)

        prod_result = response.xpath(
            u'//p/a[@title="Lodówki"]').xpath('@title').extract()

        if len(prod_result) == 0:
            print '-' + response.url
            return

        print '+' + response.url

        fridge_label = response.css('h1::text').extract()[0].split(' ')
        fridge = Product(
            url=response.url,
            name=' '.join([sstrip(i) for i in fridge_label[1:]]),
            vendor=sstrip(fridge_label[0]),
            vendor_code=sstrip(fridge_label[-1]),
            price=response.css(".selenium-price-normal").xpath(
                'text()').extract()[0].replace(u'\xa0', ''))

        for idx, d in enumerate(response.xpath(
                '//*[@class="basic-tech-details"]').xpath('.//tr')):
            row = [sstrip(i) for i in d.css('*::text').extract()
                   if sstrip(i) != '']

            for k, v in OleSpider.item_map.items():
                if row[0] == k:
                    fridge[v] = row[-1]

        yield fridge
