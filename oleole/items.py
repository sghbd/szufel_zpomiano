# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import re
import scrapy
from scrapy.exporters import BaseItemExporter


class Serializer(object):
    digit = re.compile(r'\d+')
    digitf = re.compile(r'[+-]?[0-9]*[.]?[0-9]+')

    @classmethod
    def tak2bool(cls, value):
        if u'pełny' in value:
            return 2
        elif 'tyko' in value:
            return 1
        elif 'tak' in value:
            return 1
        else:
            return 0

    @classmethod
    def number(cls, value):
        return int(cls.digit.findall(value)[0])

    @classmethod
    def numberf(cls, value):
        return float(cls.digitf.findall(value)[0])


class Exporter(BaseItemExporter):
    def export_item(self, item):
        return dict(self._get_serialized_fields(item))

    def serialize_field(self, field, name, value):
        return super(Exporter, self).serialize_field(field, name, value)


class Product(scrapy.Item):
    name = scrapy.Field(serializer=unicode)
    vendor = scrapy.Field(serializer=unicode)
    vendor_code = scrapy.Field(serializer=lambda s: unicode(s).lower())
    aggre_nb = scrapy.Field(serializer=Serializer.number)
    therm_nb = scrapy.Field(serializer=Serializer.number)
    nofrost = scrapy.Field(serializer=Serializer.tak2bool)
    price = scrapy.Field(serializer=Serializer.numberf)
    capacity_refrig = scrapy.Field(serializer=Serializer.number)
    capacity_freeze = scrapy.Field(serializer=Serializer.number)
    url = scrapy.Field(serializer=str)
