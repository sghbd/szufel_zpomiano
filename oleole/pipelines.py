# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
import codecs
from items import Exporter
from db import db


class OleolePipeline(object):
    def __init__(self):
        self.exporter = Exporter()
        self.file = codecs.open('output.jl', 'w', 'utf-8')

    def process_item(self, item, spider):
        # local - debug
        data = self.exporter.export_item(item)
        self.file.write(json.dumps(data, ensure_ascii=False) + '\n')
        # db
        uid = ('%s-%s' % (data['vendor'], data['vendor_code'])).lower()
        data['uid'] = uid
        db.t_product.update_or_insert(
            db.t_product.uid == uid, **data)
        db.commit()
        return item
