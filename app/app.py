# -*- coding: utf-8 -*-
from jinja2 import Environment, FileSystemLoader
import pandas as pd
from db import db

from tornado.web import RequestHandler

from bokeh.embed import autoload_server
from bokeh.layouts import column, row
from bokeh.charts import Bar


env = Environment(loader=FileSystemLoader('templates'))


class IndexHandler(RequestHandler):
    def get(self):
        template = env.get_template('embed.html')
        script = autoload_server(url='http://localhost:5006/app')
        self.write(template.render(script=script))


def modify_doc(doc):
    rows = db(db.t_product.id > 0).select(
        orderby=db.t_product.price)
    df = pd.DataFrame(rows.as_list()).sort_values(
        by='price', ascending=False)

    bar1 = Bar(df, 'vendor', values='price', legend=None,
               agg='max', title="Najdroższe lodówki w portfolio producentów")
    bar2 = Bar(df, 'vendor', values='price', legend=None,
               agg='min', title="Najtańsze lodówki w portfolio producentów",
               color="green")
    bar3 = Bar(df, 'vendor', values='vendor_code', legend=None,
               agg='count', title="Ilość modeli w portfolio producentów",
               color="blue")
    bar4 = Bar(df, 'vendor', values='capacity_refrig', group='nofrost',
               agg='mean',
               title="Średnia pojemność chłodziarki [l] "
                     "w zależności czy lodówka ma opcję \"NoFrost\""
                     " - 2.0 pełny, 0.0 brak",
               color=["orange", "green"])

    doc.add_root(
        column(row(bar1, bar2),
               row(bar3, bar4)))
