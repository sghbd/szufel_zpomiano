import os
from pydal import DAL, Field
if not os.path.exists('database'):
    os.mkdir('database')

db = DAL('sqlite://storage.db', folder='database')
db.define_table(
    't_product',
    Field('uid', 'string', unique=True),
    Field('name', 'string'),
    Field('vendor', 'string'),
    Field('vendor_code', 'string'),
    Field('aggre_nb', 'integer'),
    Field('therm_nb', 'integer'),
    Field('nofrost', 'integer'),
    Field('price', 'float'),
    Field('capacity_refrig', 'integer'),
    Field('capacity_freeze', 'integer'),
    Field('url', 'string'),
    format='[%(id)d] %(vendor)s %(name)s')
