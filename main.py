# -*- coding: utf-8 -*-
import os


try:
    os.remove('oleole.log')
    os.remove('output.jl')
except:
    pass


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Zbiera dane na temat lodówek.')
    parser.add_argument('-m', '--mine', dest='mine', action='store_true',
                        help='Kopie dane ze strony.')
    parser.add_argument('-s', '--show', dest='show', action='store_true',
                        help='Wizualizuje dane.')
    parser.add_argument('--shell', dest='shell', action='store_true',
                        help='Otwiera interaktywną konsolę.')

    args = parser.parse_args()

    if args.mine:
        from twisted.internet import reactor
        from scrapy.crawler import CrawlerRunner
        from scrapy.utils.log import configure_logging
        from scrapy.utils.project import get_project_settings

        configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
        runner = CrawlerRunner(get_project_settings())
        d = runner.crawl('oleole')
        d.addBoth(lambda _: reactor.stop())
        reactor.run()
    if args.show:
        from app.app import modify_doc, IndexHandler
        from tornado.ioloop import IOLoop
        from bokeh.server.server import Server
        from bokeh.util.browser import view
        from bokeh.application import Application
        from bokeh.application.handlers import FunctionHandler

        bokeh_app = Application(FunctionHandler(modify_doc))

        io_loop = IOLoop.current()

        server = Server(
            {'/app': bokeh_app},
            io_loop=io_loop,
            extra_patterns=[('/', IndexHandler)])
        server.start()
        io_loop.add_callback(view, "http://localhost:5006/")
        io_loop.start()
    if args.shell:
        from db import db
        from IPython import embed
        rows = db(db.t_product.id > 0).select()
        print db.executesql('SELECT distinct vendor FROM t_product')
        embed(header=u"""
Do dzieła!! Poniżej przykłady:
>>> rows = db(db.t_product.id > 0).select()
>>> print db.executesql('SELECT distinct vendor FROM t_product')
        """)
